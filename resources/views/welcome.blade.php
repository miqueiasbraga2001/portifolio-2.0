@extends('layouts.main')

@section('titulo', 'Portifólio')

@section('conteudo')



    <a id="btn-projetos" class="btn px-4 py-3 text-light" href="/projetos">
        <i class="bi bi-trophy-fill"></i>
        <b> Projetos</b>
    </a>

    <div class="container-sm my-3 ">
        <x-folha>
            <x-pessoal.pessoal></x-pessoal.pessoal>

            <x-stecks.principal></x-stecks.principal>
        </x-folha>

    </div>
@endsection
