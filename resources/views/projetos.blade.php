@extends('layouts.main')

@section('titulo', 'Portifólio')

@section('conteudo')

    <a id="btn-projetos" class="btn px-4 py-3 text-light" href="/">
        <i class="bi bi-trophy-fill"></i>
        <b>Currículo</b>
    </a>

    <div class="container my-3">
       <x-folha>
            <x-projetos.header>
                <i class="bi bi-trophy-fill"></i>
                <span>Projetos</span>
            </x-projetos.header>

            <x-projetos.area></x-projetos.area>
       </x-folha>
    </div>
@endsection
