<header>

    <div id="name-title" class=" py-2 px-4 text-light">
        Miquéias de Sousa Braga
    </div>

    <h1 class="text-light display-5">
        {{ $slot }}
    </h1>
    
</header>