<div id="formacao" class="col-11 mt-5 animate__animated animate__fadeInLeft">
    <h4 class="text-start mb-0" style="font-weight: 600">Formação</h4>
    <hr class="mt-0">

    <div class="fs-5 text-start">
        
        <div class="mb-3 ps-2 border-start border-5 border-info">
            <span>Engenharia Mecatrônica</span> <br>
            <a class="text-info" target="_blank" href="https://faveni.edu.br" >Favenni</a> 
            <i><small>2019-2024</small></i> 
        </div>

        <div class="mb-3 ps-2 border-start border-5 border-info">
            <span>Desenvolvimento WEB Full-Steck</span> <br>
            <a class="text-info" target="_blank" href="https://www.udemy.com" >Udemy</a> 
            <i><small>2018-2021</small></i> 
        </div>

        <div class="mb-3 ps-2 border-start border-5 border-info">
            <span>Técnico de Informática</span> <br>
            <a class="text-info" target="_blank" href="https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwiy4t3EyKGBAxWPppUCHc3YDzAQFnoECCEQAQ&url=https%3A%2F%2Fwww.ifpi.edu.br%2Fsaoraimundononato&usg=AOvVaw3Wo0vTsn683asmDdBswgJp&opi=89978449" >
                IFPI
            </a> 
            <i><small>2015-2018</small></i> 
        </div>

    </div>
</div>
