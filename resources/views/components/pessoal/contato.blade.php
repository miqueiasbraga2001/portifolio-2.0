<div id="contato" class="col-11 mt-5 animate__animated animate__fadeInLeft">
    <h4 class="text-start mb-0" style="font-weight: 600">Contato</h4>
    <hr class="mt-0">
    <input class="position-absolute opacity-0" style="top: -100px" type="text" id="email-copy" value="miqueiasbraga2001@gmail.com">

    <div class="fs-5  text-start">
        <i style="font-weight: 100">

            <div x-data="{ ishover: false }" class="mb-2 link_contato">
                <div onclick="copyToClipboard()" x-on:mouseover="ishover = true" x-on:mouseout="ishover = false"
                    style="cursor: pointer">

                    <i class="bi bi-envelope w-100" :class="{ 'text-info': ishover }"></i>

                    <span id="email">miqueiasbraga2001@gmail.com</span>
                    <span id="copy" style="display: none">Copiado!</span>
                    
                </div> 
            </div>

            <div x-data="{ ishover: false }" class="mb-2 link_contato">
                <a x-on:mouseover="ishover = true" x-on:mouseout="ishover = false" target="_blank"
                    href="https://contate.me/miqueiasbraga">
                    <i class="bi bi-whatsapp" :class="{ 'text-info': ishover }"></i>
                    <span>+55 11 99707-3041</span>
                </a>
            </div>

            <div x-data="{ ishover: false }" class="mb-2 link_contato">
                <a x-on:mouseover="ishover = true" x-on:mouseout="ishover = false" target="_blank"
                    href="https://www.google.com/maps/place/Campo+Alegre+de+Lourdes,+BA,+47220-000/@-9.5168092,-43.0196824,15z/data=!3m1!4b1!4m6!3m5!1s0x77af0989b9fb79f:0xc0e9ec3aa934c0e8!8m2!3d-9.5168096!4d-43.0093827!16s%2Fg%2F1ywtx2w_k?entry=ttu">
                    <i class="bi bi-geo-alt" :class="{ 'text-info': ishover }"></i>
                    <span>Campo Alegre de Lourdes/BA</span>
                </a>
            </div>

        </i>
    </div>
</div>

<script>
    function copyToClipboard() {
        /* Seleciona o texto do input */
        const textToCopy = document.getElementById("email-copy");
        textToCopy.select();

        /* Copia o texto para a área de transferência */
        document.execCommand("copy");

        /* Remove a seleção do input */
        textToCopy.setSelectionRange(0, 99999);

        document.getElementById("email").style.display = 'none';
        document.getElementById("copy").style.display = 'inline';

        setTimeout(function () {
            document.getElementById("email").style.display = 'inline';
            document.getElementById("copy").style.display = 'none';
        }, 2000); // Oculta o texto após 2 segundos
    }
</script>
