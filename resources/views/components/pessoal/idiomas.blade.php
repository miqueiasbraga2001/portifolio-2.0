<div id="idiomas" class="col-11 mt-5 animate__animated animate__fadeInLeft">
    <h4 class="text-start mb-0" style="font-weight: 600">Idiomas</h4>
    <hr class="mt-0">

    <div class="fs-5 text-start">
        
        <div class="mb-3 ps-2 border-start border-5 border-secondary">
            <h5 class="mb-0">Português</h5>
            <small>Nativo</small> 
        </div>

        <div class="mb-3 ps-2 border-start border-5 border-secondary">
            <h5 class="mb-0">Inglês</h5>
            <small>Básico</small> 
        </div>

    </div>
</div>
