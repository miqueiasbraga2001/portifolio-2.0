<div id="pessoal" class="col-lg-4 col-md-6 px-3 py-5 row justify-content-center text-white">

    <div class="col-6 align-self-center">
        <div id="perfil" class="m-auto rounded-circle animate__animated animate__fadeIn "></div>
    </div>

    <div class="col-11 animate__animated animate__fadeInDown">
        <h1>Miquéias de Sousa Braga</h1>
        <span class="badge fs-6 rounded-pill  border border-light p-2 mb-2">Engenheiro Mecatrônico</span>
        <span class="badge fs-6 rounded-pill border border-light p-2 mb-2">Desenvolvedor</span>
    </div>

    <x-pessoal.contato></x-pessoal.contato>

    <x-pessoal.social></x-pessoal.social>

    <x-pessoal.formacao></x-pessoal.formacao>

    <x-pessoal.idiomas></x-pessoal.idiomas>
 
</div>
