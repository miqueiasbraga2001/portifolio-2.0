<div id="social" class="col-11 mt-5 animate__animated animate__fadeInRight">
    <h4 class="text-start mb-0" style="font-weight: 600">Redes Sociais</h4>
    <hr class="mt-0">

    <div class="row fs-2 pt-2">
        <div class="col">
            <div class=" btn btn-light w-100 fs-3 p-2 "> 
                <a class="text-primary" href="https://www.instagram.com/miqueias2001sb/" target="_blank" ><i class="bi bi-instagram"></i></a>
            </div>
        </div>

        <div class="col">
            <div class=" btn btn-light w-100 fs-3 p-2">
                <a class="text-primary" href="https://www.linkedin.com/in/miquéias-braga/" target="_blank" ><i class="bi bi-linkedin"></i></a>
            </div>
        </div>

        <div class="col">
            <div class=" btn btn-light w-100 fs-3 p-2">
                <a class="text-primary" href="https://gitlab.com/miqueiasbraga2001" target="_blank" ><i class="fa-brands fa-gitlab"></i></a>
            </div>
        </div>
    </div>
</div>
