<svg xmlns="http://www.w3.org/2000/svg" width="24" height="20" viewBox="0 0 185 99" fill="none">
    <path d="M136.035 93.0981H48.7186L5.35925 49.7388L48.8679 5.93115L92.6755 49.7388M136.035 93.0981L179.394 49.7388L136.035 6.37949L92.6755 49.7388M136.035 93.0981L92.6755 49.7388" stroke="white" stroke-width="10" stroke-linejoin="round"/>
    <rect x="97.4692" y="49.5146" width="57.409" height="53.595" transform="rotate(-45 97.4692 49.5146)" fill="white"/>
  </svg>