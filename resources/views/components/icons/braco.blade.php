<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 48 48" fill="none">
    <mask id="mask0_591_62" style="mask-type:luminance" maskUnits="userSpaceOnUse" x="2" y="4" width="43" height="40">
      <path d="M5 35C5 34.4696 5.21071 33.9609 5.58579 33.5858C5.96086 33.2107 6.46957 33 7 33H41C41.5304 33 42.0391 33.2107 42.4142 33.5858C42.7893 33.9609 43 34.4696 43 35V42H5V35Z" fill="white" stroke="white" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M42 18H34L28 12L34 6H42" stroke="white" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
      <path d="M8 16C10.2091 16 12 14.2091 12 12C12 9.79086 10.2091 8 8 8C5.79086 8 4 9.79086 4 12C4 14.2091 5.79086 16 8 16Z" fill="white" stroke="white" stroke-width="4"/>
      <path d="M12 12H28M10 16L18 33" stroke="white" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"/>
    </mask>
    <g mask="url(#mask0_591_62)">
      <path d="M0 0H48V48H0V0Z" fill="white"/>
    </g>
  </svg>