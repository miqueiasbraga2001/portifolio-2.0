<div class="row justify-content-center my-md-5" >
   <div class="col-md-11 col-12 text-center ">
       <div class="row justify-content-center documento rounded rounded-3 shadow-lg ">
           {{ $slot }}
       </div>
   </div>
</div>