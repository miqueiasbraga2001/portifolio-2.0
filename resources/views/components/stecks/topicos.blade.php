<div class="row mb-5">

    @empty($icon)
        <div class="text-primary">
            <h3 class="ps-1 border-start border-5 border-primary"><b>{{ $titulo }}</b></h3>
        </div>
    @else
        <div class="text-primary">
            <h3 class="ps-1">
                <i class="{{ $icon }} "></i>
                <b>{{ $titulo }}</b>
            </h3>
        </div>
    @endif

    <div class="fs-5">
        {{ $slot }}
    </div>
    
</div>
