<div id="principal" class="col-lg-8 col-md-6 px-5 py-5 text-start">

    <x-stecks.topicos titulo="Perfil" icon=''> 
        <p>Experiente e Versátil em Engenharia e Tecnologia, com Amplas Habilidades em Desenvolvimento de Sistemas.</p>
        <p>Desenvolvo soluções web e tenho tambem a capacidade de fazer projetos físicos de controle com embarcados e
            sistemas eletromecânicos</p>
    </x-stecks.topicos>

    <hr class="mt-0 mx-2">

    <x-stecks.topicos titulo="Engenharia Mecatrônica" icon='fa-solid fa-robot'>

        <div class="row mt-2 mb-3">

            <div class="col-12 pe-0">
                <x-stecks.techLinks nome='Modelagem 3D' color='jquery' link='https://thangs.com/designer/MiquéiasSB'>
                    <i class="fa-solid fa-cubes"></i>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='Microcontroladores' color='secondary' link='https://victorvision.com.br/blog/o-que-e-um-microcontolador/'>
                    <x-icons.micro></x-icons.micro>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='Sistemas Mecânicos' color='laravel' link=''>
                    <i class="bi bi-gear-wide-connected"></i>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='Eletromecânicos' color='livewire' link=''>
                    <x-icons.motor></x-icons.motor>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='Robótica' color='alphinejs' link=''>
                    <x-icons.braco></x-icons.braco>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='Arduino' color='arduino' link='https://www.arduino.cc'>
                    <x-icons.arduino></x-icons.arduino>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='Interface Homem-Máquina' color='info' link=''>
                    <i class="bi bi-hand-index-thumb-fill"></i>
                </x-stecks.techLinks>
                
            </div>

           
        </div>

       
    </x-stecks.topicos>

    <hr class="mt-0 mx-2">

    <x-stecks.topicos titulo="Desenvolvimento WEB full-steck" icon='fa-solid fa-code'>

        <div class="row mt-2 mb-3">

            <div class="col-12 pe-0">
                <x-stecks.techLinks nome='PHP' color='php' link='https://www.php.net'>
                    <i class="fa-brands fa-php"></i>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='JavaScript' color='js' link='https://developer.mozilla.org/pt-BR/docs/Web/JavaScript'>
                    <i class="fa-brands fa-js"></i>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='Laravel' color='laravel' link='https://laravel.com'>
                    <i class="fa-brands fa-laravel"></i>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='Livewire' color='livewire' link='https://laravel-livewire.com'>
                    <x-icons.livewire></x-icons.livewire>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='Alphine.js' color='alphinejs' link='https://alpinejs.dev'>
                    <x-icons.alphinejs></x-icons.alphinejs>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='Bootstrap' color='bootstrap' link='https://getbootstrap.com'>
                    <i class="bi bi-bootstrap"></i>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='MySQL' color='mysql' link='https://www.mysql.com'>
                    <x-icons.mysql></x-icons.mysql>
                </x-stecks.techLinks>

                <x-stecks.techLinks nome='JQuery' color='jquery' link='https://jquery.com'>
                    <x-icons.jquery></x-icons.jquery>
                </x-stecks.techLinks>

                
            </div>

           
        </div>

       
    </x-stecks.topicos>

    <hr class="mt-0 mx-2">

    <x-stecks.topicos titulo="Experiência" icon=''>
        <div class="mt-2 ms-2">
            <span class="h5">CNC Laser</span> <small><i>- 08/2023</i></small>
            <ul>
                <li>Uma máquina controlada por computador que utiliza um laser para realizar cortes, gravações ou marcações precisas em diversos materiais.</li>
            </ul>
        </div>

        <div class="mt-4 ms-2">
            <span class="h5">Sistema de Pesquisa Empresarial</span> <small><i>- 07/2023</i></small>
            <ul>
                <li>Desenvolvimento autônomo de sistema de pesquisa personalizado para empresa anônima.</li>
                <li>Criação de sistema que retorna resultados e recomendações para os usuários, além de painel de administração exibindo respostas, gráficos e recomendações individuais de cada usuário.</li>
            </ul>
        </div>

        <div class="mt-4 ms-2">
            <span class="h5">Desenvolvimento de Robô e Programação de Microcontroladores </span> <small><i>- 06/2023</i></small>
            <ul>
                <li>Participação em competição de luta de robôs sumô na faculdade de Mecatrônica, responsável pela programação do controle e do robô utilizando o ESP8266 como microcontrolador.</li>
            </ul>
        </div>

        <div class="mt-4 ms-2">
            <span class="h5">Porta NAF</span> <small><i>- 10/2022</i></small>
            <ul>
                <li>Projeto acadêmico para coleta e organização de informações de atendimentos da rede NAF.</li>
                <li>Centralização dos links dos serviços prestados em um único lugar.</li>
            </ul>
        </div>
    </x-stecks.topicos>
</div>
